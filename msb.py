
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize


def var_o(sigma, A, T, N):            # frequency (omega) CRLB variance given sigma, amplitude, sample period and number of samples
    return 12 * sigma**2 / (A**2 * T**2 * N * (N**2 - 1))


def var_fi(sigma, n0, N, A):        #   phase variance CRLB given sigma, first sample index, number of samples and amplitude
    P = N * (N - 1) / 2
    Q = N * (N - 1) * (2 * N - 1) / 6
    retvar = 12 * sigma**2 * (n0**2 * N + 2 * n0 * P + Q) / (A**2 * N**2 * (N**2 -1))    
    return np.abs(retvar)


def find_sigma_from_SNR(SNR, A):    #   finds sigma give SNR and amplitude of the sine
    abs_SNR = 10**(SNR/10)
    return np.sqrt(A**2 / (2 * abs_SNR))


def signaltest(SNR, M):    # function to find phase and frequency given the SNR and fft-size M
    sigma = find_sigma_from_SNR(SNR, a)         
    noise = np.random.normal(scale = sigma, size = N) + 1j * np.random.normal(scale = sigma, size = N)  # make noise signal
    n = np.arange(n0, N + n0)
    clean_signal = a * np.e**(1j*(o*T*n + fi))  #   make sinus signal

    signal = clean_signal + noise       #   sinus signal with noise
    
    signal = np.append(signal, np.zeros(M - len(signal)))   #   zero-padding for longer FFT-size
    n = np.arange(n0, M + n0)


    fft = (np.fft.fftshift(np.fft.fft(signal))) / N     #   du fft
    fft_abs = np.abs(np.copy(fft))      #   absolute value for using argmax(fft), it only takes real numbers

    o_hat = (np.argmax(fft_abs) - M / 2) * Fs * 2 * np.pi / M    #  find maximum value of fft
    noe = np.e**(-1j*o_hat*n0*T) * fft[np.argmax(fft_abs)]  
    fi_hat = np.arctan(np.imag(noe)/np.real(noe))       #   calculate the phase, important to use the complex fft here


    return(o_hat, fi_hat)


def var(arr, my):       #   variance of array given the expected value my
    tot = 0
    N = len(arr)
    for i in range(N):
        tot += (arr[i] - my)**2
    return tot/N


n0 = -256
N = 513

a = 1       #   parameters for the signal
f0 = 10**5
o = 2 * np.pi * f0
fi = np.pi / 8
T = 10 ** -6
Fs = 10 ** 6


SNR = [-10, 0, 10, 20, 30, 40, 50, 60]
FFT_size = [10, 12, 14, 16, 18, 20]
test_size = 100                     # number of tests to run for each FFT-size for each SNR


o_plots = []
fi_plots = []

for fft_size in FFT_size:       #   Finds variance of phase and frequency estimaton for all SNR for all FFT-sizes with a test size of 100
    o_plot = []
    fi_plot = []
    for snr in SNR:
        o_array = []
        fi_array = []
        for i  in range(test_size):
            measure = signaltest(snr, 2**fft_size)      # do test
            o_array.append(measure[0])
            fi_array.append(measure[1])
        o_plot.append(var(o_array, o))      #find variance
        fi_plot.append(var(fi_array, fi))
    o_plots.append(o_plot)      # put into right array
    fi_plots.append(fi_plot)

np.savetxt('data/o_plots.csv', o_plots)     # save to file, so that
np.savetxt('data/fi_plots.csv', fi_plots)


fi_CRLB = []
for i in SNR:
    sigma = find_sigma_from_SNR(i, a)       # find the CRLB values for the same SNR
    fi_CRLB.append(var_fi(sigma, n0, N, a))


o_CRLB = []
for i in SNR:
    sigma = find_sigma_from_SNR(i, a)
    o_CRLB.append(var_o(sigma, a, T, N) )


np.savetxt('data/fi_crlb.csv', fi_CRLB)     #save these as well
np.savetxt('data/o_crlb.csv', o_CRLB)


print('done')   # always nice to know when it's time to plot ;)


