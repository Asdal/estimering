## Estimering prosjekt våren 2020

prosjektoppgave i forbindelse med faget TTT4175 Estimation, Detection and Classification ved NTNU


Prosjektet inneholder tre kjørbare filer:
- msb.py - finner variansen til estimatorene for fase og frekvens (+CRLB) og lagrer til fil
- oppg_b.py - Bruker minimering for å finne fase og frekvens og lagrer variansen til estimatorene til fil
- plot.py - leser fra fil og plotter