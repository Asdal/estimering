
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize



def find_sigma_from_SNR(SNR, A):    #   same as in msb.py
    abs_SNR = 10**(SNR/10)
    return np.sqrt(A**2 / (2 * abs_SNR))



n0 = -256
N = 513

a = 1       #   parameters for the signal
f0 = 10**5
o = 2 * np.pi * f0
fi = np.pi / 8
T = 10 ** -6
Fs = 10 ** 6

class Oppg_b:           #   needed a "public" signal, so decided to go with a private signal inside a class instead
    def __init__(self, SNR, M):
        self.SNR = SNR
        self.M = M
        self.sigma = find_sigma_from_SNR(SNR, a)    #init, setting som values


    def make_signal(self):      #   make the sinus signal with noise just as in msb.py
        sigma = find_sigma_from_SNR(self.SNR, a)
        noise = np.random.normal(scale = sigma, size = N) + 1j * np.random.normal(scale = sigma, size = N)
        n = np.arange(n0, N + n0)
        clean_signal = a * np.e**(1j*(o*T*n + fi))

        self.signal = clean_signal + noise

   
    def fft(self, signal2):
        fft = (np.fft.fftshift(np.fft.fft(signal2))) / N     #  fft just as in msb.py, but finds only frequency-estimate
        fft_abs = np.abs(np.copy(fft))

        o_hat = (np.argmax(fft_abs) - self.M / 2) * Fs * 2 * np.pi / self.M 
        return o_hat


    def single_freq_fft(self, omega):       #   the function that is to be minimized
        tot = 0
        for i in range(len(self.signal)):
            tot+=self.signal[i] * np.e**(-1j * omega * i * T)
        return -np.abs(tot) / len(self.signal)      #does not work with complex numbers


    def signaltest_minimize(self):
        
        signal2 = np.append(self.signal, np.zeros(self.M - len(self.signal)))   #   zero-padding to M length (2**10)

        guess = self.fft(signal2)   # initial guess with the 2**10 fft

        res = minimize(self.single_freq_fft, guess, method='Nelder-Mead', tol=1)    #   minimize the fourier transform, this is slow, as my implementation of a fourier transform is rubbish
        o_hat = res.x[0]    #   the x-value of the result, this is the frequency estimate

        tot = 0         
        for i in range(len(self.signal)):
            tot+=self.signal[i] * np.e**(-1j * o_hat * i * T)   #   once more for the correct frequency, just to keep the complex number
        F_of_o_hat = tot / len(self.signal)

        noe = np.e**(-1j*o_hat*n0*T) * F_of_o_hat       # need the complex number here
        fi_hat = np.arctan(np.imag(noe)/np.real(noe))

        return o_hat, fi_hat


def var(arr, my):   #   variance estimate given an array and expected value my
    tot = 0
    N = len(arr)
    for i in range(N):
        tot += (arr[i] - my)**2
    return tot/N
    


SNR = [-10, 0, 10, 20, 30, 40, 50, 60]
test_size = 100



o_plot = []
fi_plot = []        #   for all the SNR-values
for i in SNR:
    o_arr = []
    fi_arr = []
    for j in range(test_size):
        test = Oppg_b(i, 2**10)
        test.make_signal()
        o_hat, fi_hat = test.signaltest_minimize()
        o_arr.append(o_hat)
        fi_arr.append(fi_hat)
    o_plot.append(var(o_arr, o))
    fi_plot.append(var(fi_arr, fi))


np.savetxt('data/fi_minimize.csv', fi_plot) #save to file
np.savetxt('data/o_minimize.csv', o_plot)

print('done')



