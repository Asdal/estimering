
import matplotlib.pyplot as plt
import numpy as np


o_plots = np.loadtxt('data/o_plots.csv')
fi_plots = np.loadtxt('data/fi_plots.csv')      # read values from files
fi_CRLB = np.loadtxt('data/fi_crlb.csv')
o_CRLB = np.loadtxt('data/o_crlb.csv')
o_minimized = np.loadtxt('data/o_minimize.csv')
fi_minimized = np.loadtxt('data/fi_minimize.csv')


SNR = [-10, 0, 10, 20, 30, 40, 50, 60]
FFT_size = [10, 12, 14, 16, 18, 20]


for i in range(len(FFT_size)):
    plt.plot(SNR, o_plots[i], label='$2^{'+str(FFT_size[i])+'}$')   #plot variance of frequency-estimates
plt.plot(SNR, o_CRLB, label = 'CRLB')
plt.plot(SNR, o_minimized, label = '$2^{10}$ minimized')
plt.yscale('log')
plt.legend()
plt.title('Frequency variance as a function of SNR for different FFT lengths')
plt.show()


for i in range(len(FFT_size)):
    plt.plot(SNR, fi_plots[i], label='$2^{'+str(FFT_size[i])+'}$')  # plot variance of phase-estimates
plt.plot(SNR, fi_CRLB, label = 'CRLB')
plt.plot(SNR, fi_minimized, label = '$2^{10}$ minimized')
plt.yscale('log')
plt.legend()
plt.title('Phase variance as a function of SNR for different FFT lengths')
plt.show()